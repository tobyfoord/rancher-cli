[![build status](https://gitlab.com/tobyfoord/rancher-cli/badges/master/build.svg)](https://gitlab.com/tobyfoord/ci-test/commits/master)

Docker image containing the [Rancher CLI](http://rancher.com/docs/rancher/v1.6/en/cli/)